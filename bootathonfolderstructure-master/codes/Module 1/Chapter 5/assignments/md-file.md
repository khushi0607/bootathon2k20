# Series RL Circuits
_
_
<!--heading-->
## AIM 
_
<!--blockquote-->
>To design Series RL circuit and find out the current flowing thorugh each component.

## Theory:

>When we apply an ac voltage to a series RL circuit as shown below, the circuit behaves in some ways the same as the series RC circuit, and in some ways as a sort of mirror image. For example, current is still the same everywhere in this series circuit. VR is still in phase with I, and VL is still 90° out of phase with I. However, this time VL leads I — it is at +90° instead of -90°.

>For this circuit, we will assign experimental values as follows: R = 25Ω, L = 1 H and VAC = 10 Vrms. We build the circuit and measure 9.29 V across L, and 3.7 V across R. As we might have expected, this exceeds the source voltage by a substantial amount and the phase shift is the reason for it.
 
>The vectors for this example circuit are shown to the right. This time the composite phase angle is positive instead of negative, because VL leads IL . But to determine just what that phase angle is, we must start by determining XL and then calculating the rest of the circuit parameters.

>XL=2*3.14*fL 

  =6.28*10*1

  =62.8


>Z=25+j*62.8 ohm
  
  =(25^2+62.8^2)^(1/2)

  =(625+394384)^(1/2)

  =(4568.84^(1/2))

  =67.59 ohm

  >E/Z

    = 10/67.59
    =0.1479A
    =0.15A

>Vr=I*R

   =0.14795*25

   3.7V

   >Vl=I*Xl

     =0.14795*62.8

     =9.29V

     phi=arctan(Xl/R)

     phi=arctan(62.8/25)

     arctan(2.512)

     68.29

>This can be easily verified using the simulator, by creating the above mentioned circuit and measuring the current and voltages across the resistor and inductor.
_


Components:
_
 

Resistor: A resistor is a two-terminal electronic component that produces a voltage across its terminals that is proportional to the electric current through it in accordance with Ohm's law.
 
Lamp: A lamp is a replaceable component such as an incandescent light bulb, which is designed to produce light from electricity. These components usually have a base of ceramic, metal, glass or plastic, which makes an electrical connection in the socket of a light fixture.
 
Wire: A wire is a single, usually cylindrical, elongated string of metal. Wires are used to bear mechanical loads and to carry electricity and telecommunications signals. Wire is commonly formed by drawing the metal through a hole in a die or draw plate.
 
Switch: In electronics, a switch is an electrical component that can break an electrical circuit, interrupting the current or diverting it from one conductor to another.
 
Battery: In electronics, a battery or voltaic cell is a combination of many electrochemical Galvanic cells of identical type to store chemical energy and to deliver higher voltage or higher current than with single cells.
 
Voltmeter: A voltmeter is an instrument used for measuring the electrical potential difference between two points in an electric circuit. Analog voltmeters move a pointer across a scale in proportion to the voltage of the circuit; digital voltmeters give a numerical display of voltage by use of an analog to digital converter.
 
Ammeter: An ammeter is a measuring instrument used to measure the electric current in a circuit. Electric currents are measured in amperes (A), hence the name.
 
Non-contact ammeter: A type of ammeter that need not be a part of the circuit.
 

## Procedure
 

1. Select the components from the right side of the simulator and connections are made as in the circuit diagram shown in Fig:1 in the theory.

2. Connection  is complete only when the black color appears at the  end of the wires.